import { Component, OnInit } from '@angular/core';
import { GoogleChartComponent } from 'angular-google-charts';  

@Component({
  selector: 'line-graph',
  templateUrl: './line-graph.component.html',
  styleUrls: ['./line-graph.component.css']
})
export class LineGraphComponent{ 

 
  title = 'Proto Station Temperature';
  title2 = 'Salish Station Temperature'
  type = 'LineChart';
  data = [
     ["Jan",  49.982967227253816, 0.6950456840780601],
     ["Feb",  49.982967227253816, 0.6950456840780601],
     ["Mar",  49.982967227253816, 0.6950456840780601],
     ["Apr", 	45.6205664330547, 0.2562989281347805],
     ["May", 	45.6205664330547, 0.2562989281347805],
     ["Jun", 	45.6205664330547, 0.2562989281347805],
     ["Jul", 	45.6205664330547, 0.2562989281347805],
     ["Aug", 	94.29135026717677, 0.507014237677152],
     ["Sept", 	94.29135026717677, 0.507014237677152],
     ["Oct", 	94.29135026717677, 0.507014237677152],
     ["Nov", 	94.29135026717677, 0.507014237677152],
     ["Dec", 	94.29135026717677, 0.507014237677152],
  ];
  data2 = [

    ["Jan",  53.24673408290075, 0.44732458910037676],
    ["Feb",  53.24673408290075, 0.44732458910037676],
    ["Mar",  53.24673408290075, 0.44732458910037676],
    ["Apr",  53.24673408290075, 0.44732458910037676],
    ["May",  7.717943895379498, 0.3423317979283924],
    ["Jun",  7.717943895379498, 0.3423317979283924],
    ["Jul",  7.717943895379498, 0.3423317979283924],
    ["Aug",  7.717943895379498, 0.3423317979283924],
    ["Sept", 	96.92419758835133, 0.875304087295173],
    ["Oct", 	96.92419758835133, 0.875304087295173],
    ["Nov", 	96.92419758835133, 0.875304087295173],
    ["Dev", 	96.92419758835133, 0.875304087295173],
  ]

  columnNames = ["Month", "Surface Temp.","6' Temp."];
  options = {   
     hAxis: {
        title: 'Month'
     },
     vAxis:{
        title: 'Temperature'
     },
  };
  width = 550;
  height = 400;  












}
